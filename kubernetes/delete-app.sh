#!/bin/bash

kubectl delete ingress craftingjava-ingress
kubectl delete service craftingjava-redirector-service
kubectl delete deployment craftingjava-redirector-deployment

kubectl get all
