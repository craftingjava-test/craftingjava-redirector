#!/bin/bash

set -e

TICKET_NUMBER=$1
VERSION=$2

if [[ -z ${VERSION} || -z ${TICKET_NUMBER} ]]; then
  echo "Usage: {0} REDIR-NNN <version>."
  exit 1
fi

VERSION_FILE=$(find -name "Version.java")

if [[ ! -f ${VERSION_FILE} ]]; then
  echo "Version.java was not found."
  exit 1
fi

# Change project version
./mvnw versions:set -DnewVersion=${VERSION}

# Replace version in Version.java
sed -i "s/\(%%\).*\(%%\)/\1${VERSION}\2/g" ${VERSION_FILE}

# Commit changes
git commit -am "${TICKET_NUMBER} Set version to ${VERSION}"

# Tag release
if [[ ! ${VERSION} =~ SNAPSHOT$ ]]; then
  git tag ${VERSION}
fi
