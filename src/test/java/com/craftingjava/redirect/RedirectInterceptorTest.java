package com.craftingjava.redirect;

import static com.craftingjava.redirect.RedirectInterceptor.HEADER_XFF;
import static java.time.ZoneOffset.UTC;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.springframework.http.HttpHeaders.LOCATION;
import static org.springframework.http.HttpHeaders.REFERER;
import static org.springframework.http.HttpHeaders.USER_AGENT;
import static org.springframework.http.HttpStatus.MOVED_PERMANENTLY;
import static org.springframework.mock.web.MockHttpServletRequest.DEFAULT_REMOTE_ADDR;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import javax.servlet.http.HttpServletResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(MockitoJUnitRunner.class)
public class RedirectInterceptorTest {

  private static final Instant FIXED_INSTANT = Instant.parse("2007-12-03T10:15:30.00Z");
  private static final String REQUEST_URI = "https://springuni.com/";
  private static final String LOCATION_VALUE = "https://craftingjava.com/";
  private static final String REFERER_VALUE = "https://facebook.com";
  private static final String USER_AGENT_VALUE = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36";
  private static final String XFF_ADDRESS = "10.10.10.10";

  @Mock
  private ApplicationEventPublisher applicationEventPublisher;

  private RedirectInterceptor redirectInterceptor;

  private MockServletContext servletContext;
  private MockHttpServletRequest request;

  @Before
  public void setUp() {
    Clock fixedClock = Clock.fixed(FIXED_INSTANT, UTC);
    redirectInterceptor = new RedirectInterceptor(applicationEventPublisher, fixedClock);

    servletContext = new MockServletContext();
    request = MockMvcRequestBuilders.get(REQUEST_URI).buildRequest(servletContext);
  }

  @Test
  public void shouldEmitRedirectEvent() {
    request.addHeader(REFERER, REFERER_VALUE);
    request.addHeader(USER_AGENT, USER_AGENT_VALUE);
    request.setRequestURI(REQUEST_URI);

    HttpServletResponse response = new MockHttpServletResponse();
    response.addHeader(LOCATION, LOCATION_VALUE);
    response.setStatus(MOVED_PERMANENTLY.value());

    redirectInterceptor.afterCompletion(request, response, null, null);

    ArgumentCaptor<RedirectEvent> redirectEventArgumentCaptor =
        ArgumentCaptor.forClass(RedirectEvent.class);

    verify(applicationEventPublisher).publishEvent(redirectEventArgumentCaptor.capture());

    RedirectEvent redirectEvent = redirectEventArgumentCaptor.getValue();

    assertEquals(LocalDateTime.ofInstant(FIXED_INSTANT, UTC), redirectEvent.getTimestamp());
    assertEquals("GET", redirectEvent.getRequestMethod());
    assertEquals(REQUEST_URI, redirectEvent.getRequestUri());
    assertEquals(DEFAULT_REMOTE_ADDR, redirectEvent.getRemoteAddress());
    assertEquals(REFERER_VALUE, redirectEvent.getReferer());
    assertEquals(USER_AGENT_VALUE, redirectEvent.getUserAgent());
    assertEquals(MOVED_PERMANENTLY.value(), redirectEvent.getStatusCode());
    assertEquals(LOCATION_VALUE, redirectEvent.getLocation());
  }

  @Test
  public void whenExtractRemoteAddr_withoutXFF_thenReturnLocal() {
    String remoteAddr = redirectInterceptor.extractRemoteAddr(request);
    assertEquals(DEFAULT_REMOTE_ADDR, remoteAddr);
  }

  @Test
  public void whenExtractRemoteAddr_withSingleXFFAddress_thenReturnOriginal() {
    request.addHeader(HEADER_XFF, XFF_ADDRESS);
    String remoteAddr = redirectInterceptor.extractRemoteAddr(request);
    assertEquals(XFF_ADDRESS, remoteAddr);
  }

  @Test
  public void whenExtractRemoteAddr_withSingleXFFAddress_thenReturnFirst() {
    request.addHeader(HEADER_XFF, XFF_ADDRESS + ", " + XFF_ADDRESS);
    String remoteAddr = redirectInterceptor.extractRemoteAddr(request);
    assertEquals(XFF_ADDRESS, remoteAddr);
  }

}
