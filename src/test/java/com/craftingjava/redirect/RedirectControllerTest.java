package com.craftingjava.redirect;

import static com.craftingjava.redirect.RedirectController.HOST;
import static com.craftingjava.redirect.RedirectController.HTTPS;
import static org.springframework.http.HttpHeaders.CACHE_CONTROL;
import static org.springframework.http.HttpHeaders.ETAG;
import static org.springframework.http.HttpStatus.MOVED_PERMANENTLY;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.util.DigestUtils;

@WebMvcTest
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
public class RedirectControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @Test
  public void shouldRedirectToRoot() throws Exception {
    testRedirect("/", "/", MOVED_PERMANENTLY, true);
  }

  @Test
  public void shouldRedirectWithTrailingSlashToSamePath() throws Exception {
    testRedirect("/test1/test2/", "/test3/test4", MOVED_PERMANENTLY, true);
  }

  @Test
  public void shouldRedirectWithoutTrailingSlashToSamePath() throws Exception {
    testRedirect("/test1/test2", "/test3/test4", MOVED_PERMANENTLY, true);
  }

  @Test
  public void shouldRedirectUnmappedPathUnchanged() throws Exception {
    testRedirect("/unmapped-path", "/unmapped-path/", MOVED_PERMANENTLY, false);
  }

  private void testRedirect(String sourcePath, String targetPath, HttpStatus status,
      boolean shouldCache) throws Exception {
    String location = HTTPS + "://" + HOST + targetPath;
    String etag = DigestUtils.md5DigestAsHex(location.getBytes());

    ResultActions resultActions = mockMvc.perform(get(sourcePath))
        .andDo(print())
        .andExpect(status().is(status.value()))
        .andExpect(redirectedUrl(location));

    if (shouldCache) {
      resultActions
          .andExpect(header().string(CACHE_CONTROL, "max-age=86400, must-revalidate"))
          .andExpect(header().string(ETAG, "\"" + etag + "\""));
    }
  }

  @Test
  public void shouldSendMethodNotAllowedForTheRest() throws Exception {
    mockMvc.perform(post("/"))
        .andDo(print())
        .andExpect(status().isMethodNotAllowed());
  }

  @Configuration
  static class TestConfig {

    @Bean
    @ConditionalOnMissingBean
    RedirectController redirectController(Environment environment) {
      return new RedirectController(environment);
    }

  }

}
