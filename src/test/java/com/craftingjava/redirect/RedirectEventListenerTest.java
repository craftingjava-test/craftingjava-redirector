package com.craftingjava.redirect;

import static com.craftingjava.redirect.RedirectEventListener.VERSION_HEADER;
import static com.craftingjava.redirect.Version.VERSION;
import static java.time.LocalDateTime.now;
import static org.junit.Assert.assertThat;
import static org.springframework.cloud.stream.test.matcher.MessageQueueMatcher.receivesMessageThat;
import static org.springframework.integration.test.matcher.PayloadAndHeaderMatcher.sameExceptIgnorableHeaders;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.concurrent.BlockingQueue;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.cloud.stream.test.binder.MessageCollector;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class RedirectEventListenerTest {

  @Autowired
  private Source channels;

  @Autowired
  private MessageCollector collector;

  @Autowired
  private RedirectEventListener redirectEventListener;

  @Autowired
  private ObjectMapper objectMapper;

  @Test
  public void sendEvent() throws Exception {
    RedirectEvent redirectEvent = RedirectEvent.of(
        now(),
        "GET",
        "https://springuni.com",
        "127.0.0.1",
        "https://facebook.com",
        "Firefox",
        200,
        "https://craftingjava.com"
    );

    redirectEventListener.sendEvent(redirectEvent);

    BlockingQueue<Message<?>> messages = collector.forChannel(channels.output());

    Message<String> expectedMessage = MessageBuilder
        .withPayload(objectMapper.writeValueAsString(redirectEvent))
        .setHeader(VERSION_HEADER, VERSION)
        .build();

    Matcher<Message<Object>> sameExceptIgnorableHeaders =
        (Matcher<Message<Object>>) (Matcher<?>)
            sameExceptIgnorableHeaders(expectedMessage, "contentType");

    assertThat(messages, receivesMessageThat(sameExceptIgnorableHeaders));
  }

  @SpringBootApplication
  @EnableBinding(Source.class)
  static class TestApplication {

  }

}
