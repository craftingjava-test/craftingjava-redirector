package com.craftingjava.redirect;

import static java.util.concurrent.TimeUnit.HOURS;
import static org.springframework.http.HttpStatus.METHOD_NOT_ALLOWED;
import static org.springframework.http.HttpStatus.MOVED_PERMANENTLY;
import static org.springframework.http.HttpStatus.NOT_MODIFIED;
import static org.springframework.util.DigestUtils.md5DigestAsHex;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.HEAD;

import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.bind.Bindable;
import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.core.env.Environment;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.util.UriComponentsBuilder;

@Slf4j
@Controller
public class RedirectController {

  static final String SLASH = "/";
  static final String HTTPS = "https";
  static final String HOST = "craftingjava.com";
  private static final CacheControl CACHE_CONTROL =
      CacheControl.maxAge(24, HOURS).mustRevalidate();
  private static final Bindable<List<String>> REDIRECT_MAPPING_BINDABLE =
      Bindable.listOf(String.class);
  private static final String REDIRECT_MAPPING_PREFIX = "redirects";
  private final Map<String, String> redirects;

  public RedirectController(Environment environment) {
    List<String> redirectsList = Binder.get(environment)
        .bind(REDIRECT_MAPPING_PREFIX, REDIRECT_MAPPING_BINDABLE)
        .orElse(Collections.emptyList());

    redirects = redirectsList.stream()
        .map(it -> it.split("~"))
        .collect(Collectors.toMap(it -> it[0], it -> it[1]));
  }

  @RequestMapping(method = {GET, HEAD})
  public HttpEntity<?> sendRedirect(ServletWebRequest request) {
    URI originalUri = URI.create(request.getRequest().getRequestURI());

    // Use a single slash if path is missing
    String path = originalUri.getPath();
    if (StringUtils.isEmpty(path)) {
      path = SLASH;
    }

    if (!SLASH.equals(path)) {
      // Add trailing slash if missing
      if (!path.endsWith(SLASH)) {
        path = path + SLASH;
      }

      if (redirects.containsKey(path)) {
        path = redirects.get(path);
      }
    }

    URI newUri = UriComponentsBuilder.fromUri(originalUri)
        .scheme(HTTPS)
        .host(HOST)
        .replacePath(path)
        .build()
        .toUri();

    // Add ETag header
    String etag = md5DigestAsHex(newUri.toString().getBytes());
    if (request.checkNotModified(etag)) {
      return ResponseEntity.status(NOT_MODIFIED).build();
    }

    return ResponseEntity
        .status(MOVED_PERMANENTLY)
        .location(newUri)
        .cacheControl(CACHE_CONTROL)
        .build();
  }

  @RequestMapping
  public HttpEntity<?> sendMethodNotAllowed(HttpServletRequest request) {
    log.warn("{} to {} was not allowed.", request.getMethod(), request.getRequestURI());
    return ResponseEntity.status(METHOD_NOT_ALLOWED).build();
  }

}
