package com.craftingjava.redirect;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class RedirectEventListener {

  static final String VERSION_HEADER = "version";

  private final MessageChannel redirectEventChannel;

  public RedirectEventListener(Source redirectEventSource) {
    redirectEventChannel = redirectEventSource.output();
  }

  @Async
  @EventListener
  public void sendEvent(RedirectEvent redirectEvent) {
    Message<RedirectEvent> redirectEventMessage = MessageBuilder
        .withPayload(redirectEvent)
        .setHeader(VERSION_HEADER, Version.VERSION)
        .build();

    redirectEventChannel.send(redirectEventMessage);
  }

}
