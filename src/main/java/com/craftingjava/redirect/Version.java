package com.craftingjava.redirect;

public final class Version {

  public static final String VERSION;

  private static final int PREFIX_LENGTH = 2;
  private static final String _VERSION = "%%1.0.5%%";

  static {
    assert _VERSION.startsWith("%%");
    assert _VERSION.endsWith("%%");
    assert _VERSION.length() > PREFIX_LENGTH * 2;

    VERSION = _VERSION.substring(PREFIX_LENGTH, _VERSION.length() - PREFIX_LENGTH);
  }

}
