package com.craftingjava.redirect;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.ToString;

@Data(staticConstructor = "of")
@ToString(of = {"timestamp", "requestMethod", "requestUri"})
class RedirectEvent {

  // Request

  private final LocalDateTime timestamp;

  @JsonProperty("request_method")
  private final String requestMethod;

  @JsonProperty("request_uri")
  private final String requestUri;

  @JsonProperty("remote_address")
  private final String remoteAddress;

  private final String referer;

  @JsonProperty("user_agent")
  private final String userAgent;

  // Response

  @JsonProperty("status_code")
  private final int statusCode;

  private final String location;

}
