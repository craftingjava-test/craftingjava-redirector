package com.craftingjava.redirect;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class RedirectApplication {

  public static void main(String[] args) {
    SpringApplication.run(RedirectApplication.class, args);
  }

  @EnableAsync
  @Configuration
  static class AsyncConfig implements AsyncConfigurer {

    private static final int CORE_POOL_SIZE = 5;
    private static final int MAX_POOL_SIZE = 50;
    private static final int QUEUE_CAPACITY = 1000;
    private static final String THREAD_NAME_PREFIX = "worker-exec-";

    @Override
    public Executor getAsyncExecutor() {
      ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
      executor.setCorePoolSize(CORE_POOL_SIZE);
      executor.setMaxPoolSize(MAX_POOL_SIZE);
      executor.setQueueCapacity(QUEUE_CAPACITY);
      executor.setThreadNamePrefix(THREAD_NAME_PREFIX);
      executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
      executor.initialize();
      return executor;
    }

    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
      return (ex, method, params) -> {
        Class<?> targetClass = method.getDeclaringClass();
        Logger logger = LoggerFactory.getLogger(targetClass);
        logger.error(ex.getMessage(), ex);
      };
    }

  }

  @EnableBinding(Source.class)
  @Configuration
  static class BindingConfig {

  }

  @Configuration
  static class WebConfig implements WebMvcConfigurer {

    private final ApplicationEventPublisher applicationEventPublisher;

    public WebConfig(ApplicationEventPublisher applicationEventPublisher) {
      this.applicationEventPublisher = applicationEventPublisher;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
      registry.addInterceptor(new RedirectInterceptor(applicationEventPublisher));
    }

  }

}
