package com.craftingjava.redirect;

import static java.time.Clock.systemUTC;
import static org.springframework.http.HttpHeaders.LOCATION;
import static org.springframework.http.HttpHeaders.REFERER;
import static org.springframework.http.HttpHeaders.USER_AGENT;
import static org.springframework.util.StringUtils.isEmpty;
import static org.springframework.util.StringUtils.tokenizeToStringArray;

import java.time.Clock;
import java.time.LocalDateTime;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

@Slf4j
public class RedirectInterceptor extends HandlerInterceptorAdapter {

  static final String HEADER_XFF = "X-Forwarded-For";

  private final ApplicationEventPublisher applicationEventPublisher;
  private final Clock clock;

  public RedirectInterceptor(ApplicationEventPublisher applicationEventPublisher) {
    this(applicationEventPublisher, systemUTC());
  }

  /**
   * Used internally and for testing.
   */
  RedirectInterceptor(
      ApplicationEventPublisher applicationEventPublisher, Clock clock) {

    this.applicationEventPublisher = applicationEventPublisher;
    this.clock = clock;
  }

  @Override
  public void afterCompletion(
      HttpServletRequest request, HttpServletResponse response, Object handler,
      Exception ex) {

    RedirectEvent redirectEvent = RedirectEvent.of(
        LocalDateTime.now(clock),
        request.getMethod(),
        request.getRequestURI(),
        extractRemoteAddr(request),
        request.getHeader(REFERER),
        request.getHeader(USER_AGENT),
        response.getStatus(),
        response.getHeader(LOCATION)
    );

    applicationEventPublisher.publishEvent(redirectEvent);
  }

  String extractRemoteAddr(HttpServletRequest request) {
    String xffHeaderValue = request.getHeader(HEADER_XFF);
    if (isEmpty(xffHeaderValue)) {
      return request.getRemoteAddr();
    }

    String[] xffAddresses = tokenizeToStringArray(xffHeaderValue, ",", true, true);
    if (xffAddresses.length == 0) {
      return request.getRemoteAddr();
    }

    return xffAddresses[0];
  }

}
